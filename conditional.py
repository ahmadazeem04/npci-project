# if else
x = 5
y = 7

#comparison operator
if x>y:
    print(f"{x} is greater than {y}")
elif x<y:
    print(f" {x} is lesser than {y}")
else:
    print(f"{x} and{y} are equal")
#Logical operator


#Membership operator

numbers = [1,2,3,4,5]
if x in numbers:
    print(f"{x} is in numbers")
if y not in numbers:
    print(f"{y} is not in numbers")


#identity operators
if x is y:
    print(f"{x} is {y}")
if x is not y:
    print(f"{x} is not {y}")

