#Single line comment

'''
Multi line comment
You can use single or double quotes
Shortcut for commenting - Select the text and press ctrl+/
'''
x = 1       #int
y = 2.5     #float
name = "Azeem" #string
is_cool = True

#Multiple assignment
a,b,name = (1,2,"Azeem")
sum = a+b

print(sum)
print(type(sum))
