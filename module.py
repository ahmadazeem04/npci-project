#A module is basically a file containing a set of function to include in your application
# core python modules you can install using the pip package manager

import datetime

today =datetime.date.today()
print(today)

from time import time

#pip module

import camelcase
from camelcase import CamelCase

c = CamelCase()

print(c.hump('simple message'))
