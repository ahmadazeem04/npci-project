
def sayHello(name):
    print(f"Hi {name} function has been called successfully ")
sayHello('Azeem')

#return values

def getSum(num1,num2):
    sum = num1+num2
    return sum
print(getSum(2,3))

#Lambda function is a small anonymousfunction
#Lambda function can take any number of arguments

getSum = lambda num1,num2: num1+num2
print(getSum(5,7))
