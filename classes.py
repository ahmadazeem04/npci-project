
class User:
    #constructor
    def __init__(self,name,email,phone):
        self.name = name
        self.email = email
        self.phone = phone

    def greeting(self):
        return f'My name is {self.name}'

#Init user object
Azeem = User("Azeem","azeem@gmail.com","8394567898")
print(Azeem)
print(Azeem.greeting())
print(Azeem.name)

#Extend class

class Customer(User):
    def __init__(self,name,email,phone):
        self.name = name
        self.email = email
        self.phone = phone
        self.balance = 300
    def setBalance(self,balance):
        self.balance = balance

Ahmad = Customer('Ahmad','ahmad@gmail.com','8395675856')
Ahmad.selfBalance(5000)