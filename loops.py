#for loop

courses = ['Java','C','C++','Python']
for course in courses:
    if course == "C++":
        break
    print(f"Current course {course}")

#Range
for i in range(len(courses)):
    print(courses[i])

#while loop

count = 0
while count < 10:
    print(f"{count}")
    count=count+1