# A List is a collection
# which is ordered and changeable
# Allows duplicate members

#Creating list
numbers = [1,2,3,4,5]
fruits = ['Apple','Banana','Mango','Orange']

#Using constructor
num = list((1,2,3,4,5))

#Printing list
print(numbers,num)

#Get values
print(fruits[1])

#Append
fruits.append("Grapes")
print(fruits)

#Insert at a position
fruits.insert(2,'Strawberries')
print(fruits)

#sort
fruits.sort()
print(fruits)

#reverse sort
fruits.sort(reverse = True)
print(fruits)