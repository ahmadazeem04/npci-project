# A dictionary is a collection which is unordered
#Changeable and indexed
#No duplicate members

# Create dict
person = {
    'firstname' : 'Ahmad',
    'lastname' : 'Azeem',
    'age' : 23
}
print(person)
print(type(person))

#using constructor
person2 = dict(firstname = 'Ahmad',lastname = 'Azeem')
print(person2)

#Get value
print(person['firstname'])

#Add value
person['phone'] = '8356456789'
print(person)

#Get dict keys
print(person.keys())

#Get dict items
print(person.items())

#del an item

del(person['phone'])
print(person)

products = [
    {'brand' : 'Lenovo' , 'price' : 3000},
    {'brand' : 'Samsung' , 'price' : 3500},
    {'brand' : 'Dell' , 'price' : 6000},
]
print(products)