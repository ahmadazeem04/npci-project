name = "Azeem"
city = "Bareilly"

#Concatenate
#print("Hello my name is "+name+" and I am from "+city)

#Arguments by position
#print("My name is {name} and I am from {city}".format(name = name,city = city))

#F-String
#print(f"My name is {name}")

#String methods

message = "how are you?"
#Capitalize
# print(message.capitalize())

# print(message.upper())
# print(message.swapcase())
# print(len(message))
# print(message.replace('how','where'))
print(message.startswith('how'))
print(message.find('r'))