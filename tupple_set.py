#A tupple is collection which is ordered and unchangeable
#Allows duplicate members

#Create tupple
fruits = ('Apple','Banana','Grapes')
print(fruits)
print(fruits[1])

#A set is collection which is unordered and unindexed
#Donot allows duplicates

course = {'C','C++','Java'}
print(course)

print('Java' in course)

course.add('MySql')
print(course)

course.remove('MySql')
print(course)

course.clear()
print(course)

del course
print(course)
